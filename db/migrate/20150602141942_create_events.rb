class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :title
      t.text :body
      t.datetime :fecha, :null => false, :default => Time.now

      t.timestamps
    end
  end
end