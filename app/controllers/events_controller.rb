class EventsController < ApplicationController
    
    before_filter :authorize_admin, only: [:new,:create]
    #before_filter :authenticate_user! #, except: [:show, :index] #en este caso solo usuarios logueados pueden ver noticias
    
    def index
        @events = Event.all
    end

    def show
        @event = Event.find(params[:id])
    end

    def new
        @event = Event.new
    end
    
    def create
        @user = current_user
        @event = Event.new(allowed_params)

        if @event.save
            flash[:success] = "Created new event"
            redirect_to @event
        else
            render 'new'
        end
    end

    private
        def allowed_params
            params.require(:event).permit(:title, :body)
        end
end
